const app = require('express')();
var ws = require('express-ws')(app);
const port = process.env.PORT || 9000;

app.ws('/forms/abcd', (s, req) => {
    console.error('websocket connection');
    for (var t = 0; t < 3; t++)
    console.log('sending message');
        // setTimeout(() => s.send('{"type":"SET","formData":{"inputs":{"TextBox0":{"string":"ABC"}}},"transactionId":"25c71385-ec1d-43bd-9a42-065320847f07"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"GET","formData":{"inputs":{"TextBox0":{}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"DISABLE","formData":{"inputs":{"TextBox0":{}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"ENABLE","formData":{"inputs":{"TextBox0":{}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"CHANGE_TITLE","formData":{"inputs":{"title": {"string": "New title"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"CHANGE_LABEL","formData":{"inputs":{"TextBox0":{"string":"sample title"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        
        
        //run brlow togather
        s.send('{"type":"SET","formData": {"inputs":{"Table0":{"table":{"schema":[{"name":"Name"},{"name":"Age","type":"NUMBER"},{"name":"Gender"},{"name":"DOB","type":"DATETIME"},{"name":"Desc"}],"rows":[{"values":[{"string":"Test1"},{"number":"29.0"},{"string":"M"},{"string":"1993-12-31T16:45+05:30[Asia/Calcutta]"},{"string":"Description"}]},{"values":[{"string":"Test"},{"number":"28.0"},{"string":"F"},{"string":"1994-08-30T06:45+05:30[Asia/Calcutta]"},{"string":"Description"}]}]}}}},"transactionId":"25c71385-ec1d-43bd-9a42-065320847f07"}');
        // s.send('{"type":"CHANGE_TITLE","formData":{"inputs":{"title": {"string": "New title"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}');
        // setTimeout(() => s.send('{"type":"RESET","formData":{},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t); 
        // setTimeout(() => s.send('{"type":"CHANGE_LABEL","formData":{"inputs":{"TextBox0":{"string":"sample title"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
       

        // setTimeout(() => s.send('{"type":"ASSIGN","formData":{"inputs":{"Dropdown0": {"string": "a,b,c","append": "true"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"ASSIGN","formData":{"inputs":{"Dropdown0": {"string": "a,b,c","append": "false"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"ASSIGN","formData":{"inputs":{"RadioButtonGroup0": {"string": "a,b,c","append": "true"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"ASSIGN","formData":{"inputs":{"RadioButtonGroup0": {"string": "a,b,c","append": "false"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"ASSIGN","formData":{"inputs":{"CheckBoxGroup0": {"string": "a,b,c","append": "true"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"ASSIGN","formData":{"inputs":{"CheckBoxGroup0": {"string": "a,b,c","append": "false"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        
        /* 
        //run brlow togather
        s.send('{"type":"HIGHLIGHT","formData":{"inputs":{"TextBox0": {"string": "Warning","highlightType": "Warning"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{});
        setTimeout(() => s.send('{"type":"UNHIGHLIGHT","formData":{"inputs":{"TextBox0": {}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 2000*t);
         */
        // setTimeout(() => s.send('{"type":"REGISTER","formData":{"inputs":{"TextBox0": {"string": "LOSTFOCUS"}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
        // setTimeout(() => s.send('{"type":"SET_FOCUS","formData":{"inputs":{"TextBox0":{}}},"transactionId":"1dffec6a-a7c1-4b5b-8fda-bd0fa9a7c817"}', ()=>{}), 1000*t);
    }
);

/* io.on('connection', (socket) => {
  socket.on('chat message', msg => {
    io.emit('chat message', msg);
  });
}); */

app.listen(port, () => {
  console.log(`Socket.IO server running at http://localhost:${port}/`);
});
