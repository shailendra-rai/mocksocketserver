# Context
This is a mockserver created to mock the server side of socket communication needed for merged renderer. <br/>
Story: [HBC-3933](https://automationanywhere.atlassian.net/browse/HBC-3933), [parent story](https://automationanywhere.atlassian.net/browse/HBC-3929), [confluence page](https://automationanywhere.atlassian.net/wiki/spaces/SE/pages/3879829517/Form+Renderer+Convergence+Using+browser+based+forms+for+AARI+Desktop)

## Why
When testing Bot Actions on AARI web (converged code), Socker server needs to send some commands to which `AttendedPage.jsx` of `hbc-frontend-app` needs to take actions and send back responses.<br/>
Ideally this should work with a working CR instance, but in case you are facing issues doing step execution of commands through interactive forms due to device setup or some other config level. You can simply put up the mock server and change `AttendedPage.jsx` of `hbc-frontend-app` and test the attended page flow for different Bot actions.

## How to
This hosts a ws socket server on port 8282, with URL: `ws://localhost:9000/forms/abcd`, so for testing with this mock socket server relevant changes need to be made on `AttendedPage.jsx` of `hbc-frontend-app` {Look for socketurl constant}<br/>

## How to start mock server
Use below command
```
npm install
node index.js
```
On the root folder of this `mocksocketserver`
This has started the mock socket server now. 
The attended page is not designed to work with mock server so use below command to make it work with mockserver:
```
git revert d87845e
```

 - Login to localhost AARI Web
 - Now point to attended page with correct parameters for mock env: 
    - https://localhost:3001/aari#/attended?formid=**{validFormId}**&instanceid=abcd&test321=yes
    
After successfull connections to mock socket server of AARI Web, you can see logs on `mocksocketserver` of the new WS connection

## How to view socket messages
The messages can be viewer in network tab. Find network request ending with `abcd` and having status `101` 

Immediately after this connection, the mock socket server will start sending messages on the channel and `handleSocketMessage` of `AttendedPage.jsx` should start receiving these messages. And this is where you can debug the mock BOT Action commands

## Take care of below: 
 + Beware not to check-in the changed port after you changes.
 + Beware not to send more than 2 messages from the mock socket server